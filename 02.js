const figlet = require('figlet');
let text = process.argv[2];

figlet.text(text, (error, data) =>{
    if(error){
        console.log(error);
    } else {
        console.log(data);
    }
});